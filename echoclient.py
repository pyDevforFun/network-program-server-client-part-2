 
#!/usr/bin/python2.7
from twisted.internet import reactor, protocol


class Echoclient(protocol.Protocol):
    def connectionMade(self):
        self.transport.write('Hello Word!')
    def dataRecived(self, data):
        print "Server said:", data
        self.transport.loseConnection()


class EchoFactory(protocol.ClientFactory):
    def buildProtocol(self, addr):
        return Echoclient()

    def clientConnectionFailed(self, connector, reason):
        print "Connection Failed."
        reactor.stop()

    def clientConnectionLost(self, connector, reason):
        print "Connection Lost."
        reactor.stop()


reactor.connectTCP('localhost', 8000, EchoFactory())
reactor.run()
